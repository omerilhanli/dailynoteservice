package com.dailynote.service.controller;

import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dailynote.service.beans.Note;
import com.dailynote.service.beans.NoteRepository;
import com.dailynote.service.helper.HelperLog;
import com.dailynote.service.helper.HelperLog.ServiceTagEnum;

@Controller
public class NoteRetrieveController {

	@RequestMapping(method = RequestMethod.GET, value = "/get-allnotes")
	@ResponseBody
	public List<Note> getAllNotes() {

		List<Note> notes = NoteRepository.instance().getAll();

		HelperLog.log(ServiceTagEnum.GET_ALL, "Re::turned", notes != null);

		return notes;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/get-note")
	@ResponseBody
	public Note findNoteById(@RequestParam long id) {

		Note note = NoteRepository.instance().get(id);

		HelperLog.log(ServiceTagEnum.GET_ONE, String.valueOf(id), note != null);

		return note;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/post-addnote")
	@ResponseBody
	public boolean addNote(@RequestBody Note note) {

		boolean status = NoteRepository.instance().add(note);

		HelperLog.log(ServiceTagEnum.POST_ADD, note.toString(), status);

		return status;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/post-updatenote")
	@ResponseBody
	public boolean updateNote(@RequestBody Note note) {

		boolean status = NoteRepository.instance().update(note);

		HelperLog.log(ServiceTagEnum.POST_UPDATE, note.toString(), status);

		return status;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/post-remove")
	@ResponseBody
	public boolean deleteNote(@RequestParam long id) {

		boolean status = NoteRepository.instance().remove(id);

		HelperLog.log(ServiceTagEnum.POST_DELETE, String.valueOf(id), status);

		return status;
	}
}
