package com.dailynote.service.beans;

import java.util.ArrayList;
import java.util.List;

public class NoteRepository {

	private List<Note> notes = new ArrayList<>();

	private static NoteRepository instance = new NoteRepository(); // Eager init.

	public static NoteRepository instance() {

		return instance;
	}

	private NoteRepository() {
		// Initialize
		notes = new ArrayList<>();

		// Dummy
		Note note = new Note();
		note.setId(10);
		note.setContent("Hello first note");
		note.setCreatedDate("5.5.2019");
		add(note);
		// ..
	}

	public boolean add(Note note) {

		boolean isContain = verifyContain(note);

		if (isContain)
			return false;

		return notes.add(note);
	}

	public boolean update(Note noteUpdated) {

		for (int i = 0; i < notes.size(); i++) {

			Note note = notes.get(i);

			if (note.getId() == noteUpdated.getId()) {

				Note updated = notes.set(i, noteUpdated);

				return updated != null;
			}
		}

		return false;
	}

	public boolean remove(long id) {

		for (int i = 0; i < notes.size(); i++) {

			Note note = notes.get(i);

			if (note.getId() == id) {

				Note noteDeleted = notes.remove(i);

				return noteDeleted != null;
			}
		}

		return false;
	}

	public Note get(long id) {

		for (int i = 0; i < notes.size(); i++) {

			Note note = notes.get(i);

			if (note.getId() == id) {

				return note;
			}
		}

		return null;
	}

	public List<Note> getAll() {

		return notes;
	}

	private boolean verifyContain(Note note) {

		for (int i = 0; i < notes.size(); i++) {

			Note note1 = notes.get(i);

			if (note1.getId() == note.getId()) {

				return true;
			}
		}

		return false;
	}
}
