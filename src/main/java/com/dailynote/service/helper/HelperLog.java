package com.dailynote.service.helper;

public class HelperLog {

	private static final String TEXT_BREAK = " ::: ";
	private static final String TEXT_LEFT_PHARENTESIS = " (";
	private static final String TEXT_RIGHT_PHARENTESIS = ")";
	private static final String TEXT_SERVICE_WORKED = " service worked. + ";
	private static final String SUCCESS = "is_success: ";

	private static String KEY_ALL = "Get List";
	private static String KEY_ONE = "Get Item";
	private static String KEY_ADDITION = "Addition";
	private static String KEY_UPDATE = "Merge";
	private static String KEY_REMOVE = "Remove";

	public static void log(ServiceTagEnum serviceTag, String thing, boolean status) {

		String timeNow = HelperCurrentTime.now();

		System
			  .out
			  .println(

				timeNow + TEXT_BREAK

						+ serviceTag + TEXT_SERVICE_WORKED + thing

						+ TEXT_LEFT_PHARENTESIS + SUCCESS + status + TEXT_RIGHT_PHARENTESIS);
	}

	public static enum ServiceTagEnum {

		GET_ALL(KEY_ALL),

		GET_ONE(KEY_ONE),

		POST_ADD(KEY_ADDITION),

		POST_UPDATE(KEY_UPDATE),

		POST_DELETE(KEY_REMOVE);

		String l;

		ServiceTagEnum(String l) {
			this.l = l;
		}
	}
}
