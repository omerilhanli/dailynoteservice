package com.dailynote.service.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class HelperCurrentTime {

	private static final String PATTERN_DATE = "HH:mm:ss (MMMM dd, yyyy)"; 
	
	public static String now() {
		
		SimpleDateFormat formatter = new SimpleDateFormat(PATTERN_DATE, Locale.US);
		
		Calendar calendar = Calendar.getInstance();
		
		Date now = calendar.getTime();
		
		String txtNow = formatter.format(now);
		
		return txtNow;
	}
	
}
