package com.dailynote.service.server.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.dailynote.service")
public class DailyNoteServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyNoteServiceApplication.class, args);
	}
	
}

/*
						--- ### HELP TUTORIAL ### ---
						
	// ------
	Projeyi çalıştırmak için sistemde yüklü olması gerekenler ;
	- Java 1.7 or 1.8 or most release version,
	- Eclipse (bendeki sürüm, 4.11.0),
	- Apache Tomcat Server Application (bendeki sürüm 9.0.21 genelde eclipse ile beraber gelir.),
	- Spring Tools Add-On plugin for Eclipse IDE (bendeki sürüm 3.9.9 release)
	  Help->Eclipse Market'ten install edilebilir.
	//
	
	Proje Terminal üzerinden (Komut satırından) nasıl çalıştırılır?
		
	1) "cd /Users/omerilhanli/eclipse-workspace/DailyNoteService" (tap enter)
		
	2) "./mvnw clean package" (tap enter)
		
	3) "cd /Users/omerilhanli/eclipse-workspace/DailyNoteService/target/" (tap enter)
		
	4) "java -jar dailynoteservice-0.0.1-SNAPSHOT.jar" (tap enter)
		
		
		Aşağıdaki output alındığında, Başarılı bir şekilde servis başlatılmış olucaktır.
		
2019-06-20 13:49:03.626  INFO 17460 --- [           main] c.d.s.s.m.DailyNoteServiceApplication    : The following profiles are active: @spring.profiles.active@
2019-06-20 13:49:05.336  INFO 17460 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8084 (http)
2019-06-20 13:49:05.361  INFO 17460 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2019-06-20 13:49:05.362  INFO 17460 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.21]
2019-06-20 13:49:05.498  INFO 17460 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2019-06-20 13:49:05.498  INFO 17460 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 1764 ms
2019-06-20 13:49:05.923  INFO 17460 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2019-06-20 13:49:06.305  INFO 17460 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8084 (http) with context path ''
2019-06-20 13:49:06.312  INFO 17460 --- [           main] c.d.s.s.m.DailyNoteServiceApplication    : Started DailyNoteServiceApplication in 18.526 seconds (JVM running for 19.3)

		Postman veya herhangi bir yerden, (localden olmak üzere)
		
		String base_url = "http://localhost:8084/"
		
		base_url+service_path ile request atılabilir.
		
		Note: port numarası, src/main/resource/ klasörü altındaki application.properties içinden değiştirilebilir.
*/

